package com.lagarsoft.kafkastreams

import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.state.QueryableStoreType
import org.apache.kafka.streams.state.QueryableStoreTypes
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class TestController (val messagesStream : KafkaStreams) {

    @GetMapping("data")
    fun getAllData(): MutableMap<String, Long> {

        val counts = mutableMapOf<String, Long>()

        val store : QueryableStoreType<ReadOnlyKeyValueStore<String, Long>> = QueryableStoreTypes.keyValueStore()
        // This store must be the same as the configured in the KafkaStreamConfig
        val data = messagesStream.store( "store" , store)

        val all = data.all()

        while (all.hasNext()) {
            val value = all.next()
            counts.put(value.key, value.value)
        }

        return counts
    }

    @PostMapping("start")
    fun startStream() {
        // This starts the configured stream to start receiving messages
        messagesStream.start()
    }
}