package com.lagarsoft.kafkastreams.streams

import avro.com.lagarsoft.Message
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.kstream.*
import org.apache.kafka.streams.state.KeyValueStore
import org.apache.logging.log4j.LogManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*


@Configuration
class KafkaStreamsConfig (){

    private val logger = LogManager.getLogger(javaClass)

    @Bean(name = ["messagesStream"])
    /** Build a new topology for the streams **/
    fun kafkaStreamTopology(): KafkaStreams {
        val streamsBuilder = StreamsBuilder()

        val avroSerde = GenericAvroSerde().apply {
            configure(mapOf(Pair("schema.registry.url", "http://localhost:8081")), false)
        }

        val avroKeySerde = Serdes.StringSerde()

        // This is my topic that makes me receive events
        val messageAvroStream = streamsBuilder
                .stream("otrotopic", Consumed.with(avroKeySerde, avroSerde))

        // This is to materialize the count into a table
        val materialized : Materialized<String, Long, KeyValueStore<Bytes, ByteArray>> = Materialized.`as`("store")

        materialized.withKeySerde(Serdes.String())
        materialized.withValueSerde(Serdes.Long())

       val aggregated : KTable<String, Long> =
               messageAvroStream
                // Filter
                .filter {
                    _, value ->
                    val message = Message(
                            value["date"].toString(),
                            value["message"].toString()
                    )
                    logger.debug("Message: $message")

                    message.getDate().contains("0", true)
                }
                 // groupBy
                .groupBy({ key, value ->
                    val message = Message(
                            value["date"].toString(),
                            value["message"].toString()
                    )

                    message.getDate().substring(3, 6)},
                        // Have to change it because the Key Serde changes when the groupBy is applied
                       Serialized.with(Serdes.String(), avroSerde))
                // make the count
               .count( materialized )


        // send the count into a different topic!
        aggregated.toStream().to("count-topic", Produced.with(Serdes.String(), Serdes.Long()))

        val topology = streamsBuilder.build()

        val props = Properties()
        props["bootstrap.servers"] = "localhost:9092"
        props["application.id"] = "kafka-tutorial"
        return KafkaStreams(topology, props)
    }

}