package com.lagarsoft.kafkastreams.utils

import org.apache.kafka.common.serialization.Serdes
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroSerializer
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serializer
import java.util.*


class SpecificAvroSerde<T : org.apache.avro.specific.SpecificRecord> : Serde<T> {

    private val inner: Serde<T>

    /**
     * Constructor used by Kafka Streams.
     */
    constructor() {
        inner = Serdes.serdeFrom(KafkaAvroSerializer() as Serializer<T>, KafkaAvroDeserializer() as Deserializer<T>)
    }

    @JvmOverloads constructor(client: SchemaRegistryClient, props: Map<String, *> = mutableMapOf<String, Any>()) {
        inner = Serdes.serdeFrom(KafkaAvroSerializer(client, props) as Serializer<T>, KafkaAvroDeserializer(client, props) as Deserializer<T>)
    }

    override fun serializer(): Serializer<T> {
        return inner.serializer()
    }

    override fun deserializer(): Deserializer<T> {
        return inner.deserializer()
    }

    override fun configure(configs: Map<String, *>, isKey: Boolean) {
        inner.serializer().configure(configs, isKey)
        inner.deserializer().configure(configs, isKey)
    }

    override fun close() {
        inner.serializer().close()
        inner.deserializer().close()
    }

}