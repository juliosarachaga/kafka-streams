package com.lagarsoft.kafkastreams

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class KafkaStreamsApplication

fun main(args: Array<String>) {
	runApplication<KafkaStreamsApplication>(*args)
}

