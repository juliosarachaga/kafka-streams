package com.lagarsoft.kafkastreams.consumer

import avro.com.lagarsoft.Message
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import org.apache.avro.generic.GenericDatumWriter
import org.apache.avro.generic.GenericRecord
import org.apache.avro.io.DecoderFactory
import org.apache.avro.io.EncoderFactory
import org.apache.avro.specific.SpecificDatumReader
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.config.KafkaListenerEndpointRegistry
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream


@Service
class Listener (val registry: KafkaListenerEndpointRegistry){

    companion object {
        private val log = LoggerFactory.getLogger(Listener::class.java)
    }

    @KafkaListener(topics = ["otrotopic"], clientIdPrefix = "listenerClient", groupId = "listeners")
    fun listen(consumerRecord: ConsumerRecord<String, GenericRecord>) {

        val avroDeserializer = KafkaAvroDeserializer()

        val writer = GenericDatumWriter<GenericRecord>(Message.getClassSchema())
        val out = ByteArrayOutputStream()
        val encoder = EncoderFactory.get().binaryEncoder(out, null)
        writer.write(consumerRecord.value(), encoder)
        encoder.flush()

        val avroData = out.toByteArray()
        out.close()

        val reader = SpecificDatumReader<Message>(Message::class.java)
        val decoder = DecoderFactory.get().binaryDecoder(avroData, null)
        val message = reader.read(null, decoder)


        log.info("Aca esta la data que llega!!!! : ${message.getMessage()}")

    }



}