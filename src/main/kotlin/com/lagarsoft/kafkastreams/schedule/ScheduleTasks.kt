package com.lagarsoft.kafkastreams.schedule

import avro.com.lagarsoft.Message
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.util.*

@Component
class ScheduledTasks (val producer : KafkaTemplate<String, Message>) {

    @Scheduled(fixedRate = 1000)
    fun reportCurrentTime() {

        val date = dateFormat.format(Date())
        log.info("The time is now $date")

        val record = ProducerRecord<String, Message>("otrotopic", date, Message(date, "New message produced at $date"))

        producer.send(record)
    }

    companion object {

        private val log = LoggerFactory.getLogger(ScheduledTasks::class.java)

        private val dateFormat = SimpleDateFormat("HH:mm:ss")
    }
}